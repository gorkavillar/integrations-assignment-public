# Assignment for the freelancer
## Steps to follow
1. Create Revo tables/zones models
2. Create getRevoTables function (index.ts)
3. Create unit tests for getRevoTables function
4. The freelancer will set up a server with an endpoint (POST) which will listen to queries from the outside (getTables)
5. The freelancer will set up an authorization via a Dummy API Token
## Delivery (48 hours from the reception of the assignment)
- The freelancer will provide the code for an Express Server using Typescript
- The freelancer will provide documentation for connecting to the endpoint (token and options)
- The freelancer will provide the repository containing the freelancer's code

> Note: There is valuable info in the .env file